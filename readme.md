## How the whole process works
For this task, I will be paying for 1 hour of work. Whatever you submit here, I will assume to be your productivity level per 1 hour.
You don't need to finish the task, just work on it until you run out of time and then submit.

Work on the task in the way you would normally work, but make sure to follow these requirements:

* Code should be as readable as possible (try to follow the principles of Clean Code)
* If you want to use other software engineering practices that would add overhead, please let me know.

You're completely free to use the tools that you are familiar with.

I expect a high independence from you, I will answer a single question about the task.

**I will terminate immediately the trial task offer (I'll pay you, but I will leave a bad reference your oDesk profile) if: **
* the submission is really bad or unrelated to the task
* you fail to to the steps in the section submitting the task, I will terminate the contract immediately (I will pay you), it will be reflected on your oDesk profile.

## Task - login into playlyfe.com and make a successful request to a protected resource 
The challenge of this task is that there is no documentation or any API that allows you to easily to this. But the task can be done ;)

1. Fork this repository.
2. Open the dllTest.sln solution. There are various comments in the classes already in there, read them to better understand what the task is about.
3. Implement the methods in the IPlaylyfe interface in such a way that PlaylyfeMain.Main prints true.
4. Create a unit test that is relevant to the above implementation with the testing framework of your choice.

## Submitting the task
1. Commit and Push your code to your new repository.
2. Send me a pull request, I will review your code and get back to you.

Note: If you submit code that is obviously not related to the task, or you don't send me a pull 